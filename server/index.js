const path = require("path");
const express = require("express")
const app = express();
const PORT = 8020;
const PATH_DIR = __dirname+"/public/";

//static
app.use(express.static("public"));


app.get("/",(req,res)=>{
    res.sendFile(path.join(PATH_DIR+"index.html"))
})

app.get("/cari-mobil",(req,res)=>{
    res.sendFile(path.join(__dirname, '..', 'public', 'cariMobil.html'))
})




app.listen(PORT, ()=>console.log(`Server running at localhost:${PORT}`))
