class App {
  constructor() {
    this.clearButton = document.getElementById("clear-btn");
    this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");
  }

  async init() {
    await this.load();

    // Register click listener
    this.clearButton.onclick = this.clear;
    this.loadButton.onclick = this.run;
  }

  run = () => {
    Car.list.forEach((car) => {
      const node = document.createElement("div");
      node.className='col-4';
      node.innerHTML = car.render();
      this.carContainerElement.appendChild(node);
    });
  };

  async load(){
    this.clear();
    const jumlahPenumpang = document.getElementById('penumpang').value;
    const waktuBooking = document.getElementById('tanggal-pinjam').value;
    
     
    // if(waktuBooking>)

    const cars = await Binar.listCars();

    

    //fungsi filter
    const filterCar = cars.filter((car) => {
      // var dt = car.availableAt;
      // const availableAtNew = dt.getFullYear() + "-" + (dt.getMonth() + 1) + "-" + dt.getDate();
      // // console.log(waktuBooking);
      // // console.log(availableAtNew);
      
      // if (waktuBooking > availableAtNew){
      //   console.log("true")
      // }else{
      //   console.log("asu")
      // }

      return car.capacity >= jumlahPenumpang && car.available === true ;
    })

    Car.init(filterCar);
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}
